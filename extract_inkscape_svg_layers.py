#!/usr/bin/env python3

# Copyright (c) 2018-2019 Jean-Benoist Leger <jb@leger.tf>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import re
import os
import sys
import pathlib
import argparse
import tempfile
import subprocess
from lxml import etree


def extract_svg_layers(fin, layers, verbose):

    found_layers = []

    with open(fin, "rb") as f:
        tree = etree.fromstring(f.read())

    ns = tree.nsmap[None]
    nsinkscape = tree.nsmap["inkscape"]
    g = "{%s}g" % ns
    groupmode = "{%s}groupmode" % nsinkscape
    label = "{%s}label" % nsinkscape

    for element in tree.iter():
        if g != element.tag:
            continue
        if groupmode not in element.attrib:
            continue
        if element.attrib[groupmode] != "layer":
            continue
        if label not in element.attrib:
            continue
        layername = element.attrib[label]
        found_layers.append(layername)
        if verbose:
            print(f"Layer {layername}: ", end="", file=sys.stderr)
        if 'style' in element.attrib:
            style = {s.split(':')[0]:s.split(':')[1] for s in element.attrib["style"].split(';')}
        else:
            style = {}
        if layers is not None:
            if layername in layers:
                style['display'] = 'inline'
                if verbose:
                    print("displayed", file=sys.stderr)
            else:
                style['display'] = 'none'
                if verbose:
                    print("removed", file=sys.stderr)
            element.attrib["style"] = ';'.join(f"{k}:{v}" for k,v in style.items())
    if layers is None:
        return found_layers

    notfoundlayers = set(layers).difference(set(found_layers))
    if notfoundlayers:
        print("Layers not found: " + ", ".join(notfoundlayers), file=sys.stderr)
        sys.exit(1)

    return etree.tounicode(tree, method="xml", pretty_print=True)


def main():
    parser = argparse.ArgumentParser(
        description="display specified layers in inkscape svg, remove other layers. "
        "Output in svg, pdf, png"
    )
    parser.add_argument(
        "-o",
        "--output",
        help="Output file name. (default is constructed with orignal file name and layers)",
        type=str,
        default=None,
    )
    parser.add_argument(
        "-q", "--quiet", help="Quiet mode", default=False, action="store_true"
    )
    parser.add_argument(
        "-t", "--tex", default=False, action="store_true",
        help="LaTeX mode. With this mode, the output is interpreted as the output path (created if not exists), the output is not created if the output file exists and is newer than the original, the output file name is returned as \def on stdout, and the layer list have to be in one argument aith comma separated values."
    )
    parser.add_argument(
        "-f",
        "--format",
        help="Output format (svg, pdf, png). Default: svg.",
        type=str,
        default="svg",
    )
    parser.add_argument(
        "-v", "--verbose", help="Verbose mode", default=False, action="store_true"
    )
    parser.add_argument(
            "-r","--regex", help="use regex to select layers", default=False,
            action="store_true")
    parser.add_argument(
            "-e","--exclude", help="Layers provided in command line are layer to exclude.", default=False, action="store_true")
    parser.add_argument(dest="input", type=str, help="Input file name")
    parser.add_argument(dest="layers", type=str, nargs="*", help="Layers to display")

    args = parser.parse_args()

    if args.quiet and args.verbose:
        args.verbose = False

    if args.format not in ("svg", "png", "pdf"):
        print("Unkown format", file=sys.stderr)
        sys.exit(1)

    fninput = args.input
    fnoutput = args.output
    fakegen = False

    if args.tex:
        if args.layers:
           args.layers = args.layers[0].split(',')
    layers_provided = [l for l in args.layers if len(l)>0]

    if args.regex or args.exclude:
        found_layers = extract_svg_layers(fninput, None, verbose=args.verbose)
        if args.regex:
            layers_provided = [l for l in found_layers if any(re.match(f".*{r}", l) for r in layers_provided)]
        if args.exclude:
            layers_provided = [l for l in found_layers if l not in layers_provided]

    if args.tex:
        if args.output is None:
            print("Output must be provided in TeX mode", file=sys.stderr)
            sys.exit(1)
        if not os.path.isdir(args.output):
            if os.path.exists(args.output):
                print("Output must be a directory (which exists or not) in TeX mode", file=sys.stderr)
                sys.exit(1)
            os.mkdir(args.output)

        splitted_path = args.output.split(os.path.sep)
        if splitted_path[-1] == '':
            splitted_path.pop(-1)
        baseinput = fninput.split(os.sep)[-1]
        if baseinput[-4:] == '.svg':
            baseinput = baseinput[:-4]
        foutput = baseinput + "_" + "_".join(l.replace(" ", "").replace("_","-") for l in layers_provided)
        splitted_path.append(foutput+'.'+args.format)
        fnoutput = os.sep.join(splitted_path)

        if os.path.isfile(fnoutput):
            outputtime = os.path.getmtime(fnoutput)
            inputtime = os.path.getmtime(fninput)
            if inputtime<outputtime:
                fakegen = True


    if fnoutput is None:
        p = pathlib.Path(fninput)
        finput = str(p.with_suffix(""))
        foutput = finput + "_" + "_".join(l.replace(" ", "") for l in layers_provided)
        fnoutput = str(pathlib.Path(foutput).with_suffix("." + args.format))
    if not args.quiet:
        print(f"Output file: {fnoutput}", file=sys.stderr)

    svg = extract_svg_layers(fninput, layers_provided, verbose=args.verbose)

    if not fakegen:
        if args.format == "svg":
            with open(fnoutput, "w") as f:
                f.write(svg)
        else:
            with tempfile.NamedTemporaryFile(suffix=".svg", delete=False) as f:
                f.write(svg.encode())
                svgfile = f.name

            if args.verbose:
                print(f"Temp svg: {svgfile}", file=sys.stderr)
            cmd = ["inkscape"]
            if args.format == "pdf":
                cmd.append("--export-type=pdf")
            if args.format == "png":
                cmd.append("--export-type=png")
            cmd.append("-o")
            cmd.append(fnoutput)
            cmd.append(svgfile)
            if args.verbose:
                print("Run: " + " ".join(cmd), file=sys.stderr)
            subprocess.call(cmd)
            if args.verbose:
                print(f"Delete: {svgfile}", file=sys.stderr)
            os.remove(svgfile)
    else:
        if not args.quiet:
            print(f"[Skipped]", file=sys.stderr)

    if args.tex:
        print(r"\makeatletter")
        print(r"\def\inkscapesvg@generatedfilename{" + fnoutput + r"}")
        print(r"\makeatother")


if __name__ == "__main__":
    main()
