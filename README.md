This stuff allows inclusion of svg (from inkscape) with selection of the layers.

# Overview

## Needed

 - In the project:
   - File `inkscapesvg.sty`
   - File `extract_inkscape_svg_layers.py`
 - On the system:
   - `python3`,
   - the `lxml` module for python3,
   - `inkscape`,
   - a not too old version of `pdftex`.

## Short guide:

Add in the preamble: 

```latex
\usepackage{inkscapesvg}
```

The main command is `\includeinscapesvg`, with the following args:

 - a optionnal `*`. When present the behavior is inversed. All layers are
   included, except provided layers.
 - a optionnal argument `[...]`. When present, this argument is passed to `\includegraphics`
 - a mandatory argument `{...}`. The svg filename.
 - a optional `r`. When present, layers are interpreted as regex.
 - a optional argument `[...]` containings the comma separated layers names to include (or exclude if behavior is inversed).

Example:

 - to include only the background
   ```latex
   \includeinkscapesvg{myfile.svg}
   ```
 - to include all layers
   ```latex
   \includeinkscapesvg*{myfile.svg}
   ```
 - to include two layers which  have name `foo` and `bar`,
   use:
   ```latex
   \includeinkscapesvg{myfile.svg}[foo,bar]
   ```
 - to include all layers except layers which  have name `foo` and `bar`,
   use:
   ```latex
   \includeinkscapesvg*{myfile.svg}[foo,bar]
   ```
 - to include all layers matching the regex `fo.*r`:
   ```latex
   \includeinkscapesvg{myfile.svg}r[fo.*r]
   ```
 - to provide optional arguments to `\includegraphics`:
   ```latex
   \includeinkscapesvg[width=.73\textwidth]{myfile.svg}[foo]
   ```

To build, use the `pdflatex` command with the `-shell-escape` flag. *e.g.* to
build `example.tex`:

```sh
pdflatex -shell-escape example.tex
```

More options:

 - you can change the directory used for cache adding in the preamble:
   ```
   \renewcommand\inkscapesvgPath{mycache}
   ```

Example can be seen in file `example.tex` which use a figure built from layers
of `example.svg`.
